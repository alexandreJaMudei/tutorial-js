
function onReject(err){
    console.log(err.message)
}

let promise = new Promise((resolve, reject) => {
    setTimeout(() => {
        let err = new Error('REJECTED!')
        reject(err)
    }, 300);
}).then(null, onReject)

