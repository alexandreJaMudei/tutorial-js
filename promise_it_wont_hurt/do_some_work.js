const qhttp = require('q-io/http');

/*
line 14 should make the request do DB instead of just passing the user id to a variable
*/

function handleErr(data) {
    console.log('error')
}

new Promise((resolve, reject) => {
    let user_id = ''
    qhttp.read('http://localhost:7000')
        .then((data) => user_id = data)
        .then(() => {
            return qhttp.read('http://localhost:7001/' + user_id)
        })
        .then(data => {
            data = JSON.parse(data.toString())
            resolve(data)
        })

})
    .then(console.log, handleErr)
