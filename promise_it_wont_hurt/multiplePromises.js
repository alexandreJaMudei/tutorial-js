function all(promise1, promise2) {
    return new Promise((resolve, reject) => {
        let count = 0;
        const result = [];

        promise1.then((data) => {
            result.push(data)
            count++;
            if (count >= 2) {
                resolve(result)
            }
        })

        if (count >= 2) {
            resolve(result)
        }

        promise2.then((data) => {
            result.push(data);
            count++;
            if (count >= 2) {
                resolve(result)
            }
        })

    })
}

all(getPromise1(), getPromise2())
    .then(console.log)