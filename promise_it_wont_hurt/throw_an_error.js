const parsePromised = () => {
    let p = new Promise((resolve, reject) => {
        let jsonData = process.argv[2];
        try {
            jsonData = JSON.parse(jsonData)
            resolve(jsonData);
        }
        catch (err) {
            reject(err.message)
        }
    })
    .then(null, console.log)
}

parsePromised()