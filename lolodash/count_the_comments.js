var _ = require('lodash')

const worker = function (comments) {
    const result = []
    const grp = _.groupBy(comments, 'username')
    _.forEach(grp, (u) => {
        result.push({
            'username': u[0].username,
            'comment_count': u.length
        }
        )
    })

    return _.sortBy(result, (item) => -item.comment_count);
}

module.exports = worker;