var _ = require('lodash')

const worker = function (towns) {
    const result = { 'hot': [], 'warm': [] }

    _.forEach(towns, (temps, city) => {
        if (_.every(temps, value => value > 19)) {
            result.hot.push(city)
        }
        else if (_.some(temps, (value) => value > 19)) {
            result.warm.push(city)
        }
    })

    return result;
}

module.exports = worker;