var _ = require('lodash')

const worker = function( args){
    return _.forEach(args, function(value){
        if(value.population >= 1 ){ value.size = 'big'}
        else if(value.population < 0.5 ){ value.size = 'small'}
        else{ value.size = 'med'}
    })
}

module.exports = worker;