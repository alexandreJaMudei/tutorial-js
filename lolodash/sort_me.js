var _ = require('lodash')

// try to use a compare function as cb of sortBy
const worker = function (itemsSold) {
    return _.sortBy(itemsSold, (item) => item.quantity * -1)

}

module.exports = worker;