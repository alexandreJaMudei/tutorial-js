var _ = require('lodash')

const worker = function (words) {
    return _.chain(words)
        .map((word) => (word + 'chained').toUpperCase())
        .sortBy(function(w){return w})
}

module.exports = worker;