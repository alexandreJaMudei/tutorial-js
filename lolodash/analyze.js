var _ = require('lodash')

const worker = function (freelancers) {
    let result = {}
    let mean = 0
    // calc mean income
    _.forEach(freelancers, (person) => {
        mean += person.income
    })
    result.average = mean / _.size(freelancers)   

    //grups: bellow the mean
    result['underperform'] = freelancers.filter((person) => {
         return person.income <= result['average']
    })
    
    // above the mean
    result['overperform'] = freelancers.filter((person) => {
        return person.income > result['average']
    })

    // each result need to be sorted ascending
    result.underperform = _.sortBy(result.underperform, 'income')
    result.overperform = _.sortBy(result.overperform, 'income')

    return result;
}

module.exports = worker;