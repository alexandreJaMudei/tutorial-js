const fs = require('fs');
const path = require('path');

pathName = process.argv[2];
extName = '.' + process.argv[3];

fs.readdir(pathName, (err, files) => {
    if (!err) {
         files.forEach(file => {
            if (path.extname(file) === extName) {
                console.log(file);
            }

        });
    }
});