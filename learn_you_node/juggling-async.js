const http = require('http');

const urls = process.argv.slice(2, 5);
const data = {};
let completedResp = 0;
urls.forEach(url => {
    data[url] = [];
    storeData(url);
});

function storeData(url) {
    http.get(url, (res) => {

        res.setEncoding('utf8');
        res.on('data', (respData) => {
            data[url] += respData;
        })
        res.on('end', () => {
            completedResp++;
            if (completedResp === 3) {
               Object.keys(data).forEach((key) => {
                   console.log(data[key])
               })
            }
        })

    });
}