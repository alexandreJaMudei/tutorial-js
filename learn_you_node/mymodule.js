const fs = require('fs');
const path = require('path');

module.exports = function (pathName, extName, fn) {
    extName = '.' + extName;
    fs.readdir(pathName, (err, files) => {
        if (err) return fn(err);
        files = files.filter(file => {
            return path.extname(file) === extName;
        })
        fn(null, files);
    });
    
}
