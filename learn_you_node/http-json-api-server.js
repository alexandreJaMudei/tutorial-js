const http = require('http');
const url = require('url');

const PORT = process.argv[2];

const server = http.createServer((req, res) => {
    // req.on('readable', (data) => {
    //     console.log('received connection')
    //     res.end('end connection')})

    let date = new Date(newURL.searchParams.get('iso'))

    // primeiro endpoint
    if (newURL.pathname === '/api/parsetime') {
        const respData = {
            hour: date.getHours(),
            minute: date.getMinutes(),
            second: date.getSeconds()
        }
        res.writeHead(200, { 'Content-Type': 'application/json' })
        res.end(JSON.stringify(respData))
    }
    // segundo endpoint
    else if (newURL.pathname === '/api/unixtime') {
        const respData = {
            unixtime: Date.now()
        }
        res.writeHead(200, { 'Content-Type': 'application/json' })
        res.end(JSON.stringify(respData))
    }
})

server.listen(PORT);