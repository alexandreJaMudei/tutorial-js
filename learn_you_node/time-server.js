const net = require('net');
const PORT = process.argv[2];
const server = net.createServer((sock) =>{
        date = new Date();
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let day = date.getDate();
        let hour = date.getHours();
        let minutes = date.getMinutes();

        if(month < 10){month = '0' + month.toString();}
        if(day < 10){day = '0' + day.toString();}
        if(hour < 10){hour = '0' + hour.toString();}
        if(minutes < 10){minutes = '0' + minutes.toString();}

        resp = `${year}-${month}-${day} ${hour}:${minutes}\n`;
        // console.log(resp);
        sock.end(resp);
    });
server.on('end', () => server.close());
server.listen(PORT);
