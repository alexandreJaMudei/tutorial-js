const http = require('http');

const PORT = process.argv[2];

const server = http.createServer((req, res) => {
    if( req.method === 'POST'){
        req.on('data', (data) => {
            let upperCaseData = '';
            upperCaseData = data.toString().toUpperCase()
            res.write(upperCaseData)
        })
        req.on('end' ,() => res.end())
    }
})
server.on('end', () => server.close())
server.listen(PORT);